require 'hashie'

module ReleasePostHelpers
  def assemble_release_post(previous_milestone, next_milestone)
    current_article = OpenStruct.new
    current_article.data = OpenStruct.new
    current_article.data.title = "GitLab.com - What's New"
    current_article.data.description = "The latest features available on GitLab.com"
    current_article.data.previous_release_number = previous_milestone
    current_article.content = release_post_items('unreleased')

    if !current_article.content.empty?
      current_article.data.release_number = next_milestone + " Preview"
    else
      current_article.data.release_number = previous_milestone
      _, current_article.content = data.release_posts.except("unreleased").max
    end
    current_article
  end

  def release_post_items(milestone)
    # Stub structure into which feature blocks will be merged
    items = Hashie::Mash.new(features: { top: [], primary: [], secondary: [] }, deprecations: [])

    (data.release_posts[milestone.tr(".", "_")] || []).each do |filename, content|
      next if filename == "samples"

      # Extend each sortable blocks with filename for sorting
      sortable_blocks = [
        content.dig('features', 'top'),
        content.dig('features', 'primary'),
        content.dig('features', 'secondary'),
        content.dig('deprecations')
      ].compact.reduce([], :|)
      Array(sortable_blocks).each { |block| block['filename'] = filename }

      # Extend each sortable blocks with filename for sorting
      sortable_blocks = content.dig('features', 'top') ||
        content.dig('features', 'primary') ||
        content.dig('features', 'secondary') ||
        content.dig('deprecation')
      Array(sortable_blocks).each { |block| block['filename'] = filename }

      items.deep_merge!(content) { |key, this_val, other_val| this_val + other_val }
    end

    # Sort items alphabetically by filename
    items.features.top = items.features.top.sort_by { |item| item["filename"] }
    items.features.primary = items.features.primary.sort_by { |item| item["filename"] }
    items.features.secondary = items.features.secondary.sort_by { |item| item["filename"] }
    items.deprecations = items.deprecations.sort_by { |item| item["filename"] }

    items
  end
end
