---
layout: markdown_page
title: GitLab Meetups Checklist
suppress_header: true
---

## On this page
{:.no_toc}

- TOC
{:toc}


## GitLab Meetups Checklist 

This guide is intended to help Meetup organizers run events that their community will love. Our aim is to be as comprehensive as possible to enable everyone to become an organizer, regardless of experience. We realize that we don't have all the answers so if you find anything incorrect, notice something missing, or identify other changes to be made, please [open an issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issue) for our team to review. Happy planning! 

### Overview 

GitLab supports community leaders who want to organize meetups and tech events in their cities and hometowns. As the first single application for the entire DevOps lifecycle, all events that discuss and educate on the software development lifecycle and developer experience are eligible for [GitLab support](#how-gitlab-can-help).

Our goal in supporting these events is to increase awareness of GitLab and Concurrent DevOps, and to better educate the technology community about the power of our application.

### Who can contribute

At GitLab, we believe everyone can contribute. We support people who are interested in organizing events or growing existing communities. If you have experience organizing tech events or meetup groups, that is great - but it is not required. We're happy to work with first-time organizers, too.

The only requirements for organizers are a passion for GitLab and a belief in our [mission](https://about.gitlab.com/company/strategy/#mission).

### Why should you get involved

GitLab published a [blog post](/blog/2020/03/05/get-involved-with-gitlab-meetups/) to highlight the many reasons and ways to get involved in Meetups. The reasons for getting involved in a meetup community include:
* Connecting with your community - Meetups tend to bring together folks with similar interests and values.
* Expanding your skillset - Whether you're new to tech or looking to develop new skills, meetups are an opportunity to develop core skills needed for success - particularly communication and collaboration.
* Pursuing your passion - If you're passionate about a technology or programming language, meetups are a way to grow the interest in those topics, increase your influence in their community, and help advocate and champion them

### How GitLab can help

GitLab supports organizers of in-person and remote meetups with planning and logistics support, connections to speakers, GitLab swag, and financial support for food and beverages for your events.

## Planning an in-person meetup 

**IMPORTANT: Due to the COVID-19 pandemic, GitLab is not currently supporting in-person meetups in order to encourage responsible social distancing within our community.**

### Getting started

- When you're ready to begin planning an in-person meetup, please open an issue using our [meetups template](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=meetup-organizer). 
- If you would like to review this guide and discuss your plans with a GitLab team member, please [schedule a Zoom call](https://calendly.com/jcoghlan) with GitLab's Evangelist Program Manager.

### What to do ASAP 

- Find speakers. Need help? Use the GitLab [Find a Speaker](https://about.gitlab.com/events/find-a-speaker/) page. 
- Find a venue. Cafes, community centers, coworking spaces, and local tech companies are common venues for meetups. 
- Set the date. This requires confirming availability of both venue and speaker.
- Set up an event page. We are happy to connect your group with our [Meetup Pro network](https://www.meetup.com/pro/gitlab). Leave a comment on the event issue if you'd like to leverage our Meetup page to get your group started.  
- Apply a Code of Conduct to your group to ensure the safety and comfort of all participants. [GitLab's Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/) is a good option.
- Promote the event to your target audience. We recommend sharing on Reddit, Twitter, LinkedIn, Facebook, and other social channels to reach your audience. Use appropriate hashtags to reach people outside your network of followers. You may also want to share the event on tech mailing lists or websites that promote events in your area. 
- For GitLab team members only: follow [the steps in our marketing handbook](/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents) to add your event to [about.gitlab.com/events](/events). You can assign the merge request to the Evangelist Program Manager (@johncoghlan) to merge. 

### What to do one week before your event

- Confirm plans for the event with your speaker(s) and venue. This is a good time to discuss how the speakers plan to present. Will they be using their laptop? What type of ports does their laptop have? Will they need an adapter? Do these match what is available at the venue? 
- Recruit volunteers from your network or community to help you on the day of the event. 
- Send a reminder to your guests about the event and encourage them to help you promote it. Include a simple ask of "share this with your colleagues, friends, and on social media".

### What to do the day of your event

- Send reminder to your guests before 1200 local time. Include directions, an agenda, and any other important information: is the entrance tucked away? will they need an ID or code to enter the building? are folks welcome to arrive early or is there a set time that doors will open? 
- Print and hang signs directing attendees to the room where the event will be held and the restrooms.
- Request volunteers arrive 30 min before the start time so you can brief them on their responsibilities and answer any questions. 
- Test the AV in the room to ensure everything is in working order before guests begin to arrive. This will allow time to troubleshoot should any issues arise. It never hurts to have extra cables, adapters, batteries, etc. 
- Set up food, drinks, swag, and any other materials you have for the event. 
- Welcome your guests, share important updates with the group, review agenda, thank your host, and introduce your speakers. 
- After the event, make sure you leave the venue clean and return any loaned AV equipment. 
- Send a thank you to attendees and include a form for feedback and a reminder to RSVP for your next meetup (if one has been scheduled). 

### What to do the day after your event 

- Send thank you notes to the venue hosts, your co-organizers and volunteers, the speakers, and anyone else who helped you with the meetup. 
- If you incurred expenses related to the meetup, please follow the [Meetup expense process](/handbook/marketing/community-relations/evangelist-program/#meetup-expenses) to receive reimbursement.

### Simple tips to take your meetup to the next level 

- Signs, signs, everywhere signs: When setting up for your meetup, signs can be a big help to make sure your guests feel comfortable. Post a 'Welcome' sign near the door so folks know they are in the right place upon arrival and hang directional signs pointing your guests to the presentation space, refreshments, and restrooms. It also helps to post the wifi password and an agenda somewhere in the room (on paper, a whiteboard, or a welcome slide on the screen) so everyone knows the plan for the evening. 
- Set the mood: the atmosphere at a meetup tends to be set by the first guests to arrive. If they grab seats and jump on their phones, later arrivals tend to follow that lead. If you want a more lively meetup, make yourself available for conversation as guests begin to arrive and introduce guests to each other to keep the conversations going. Some background music playing at a low-volume can also help to prevent the library vibe. 
- Speaker swag: when possible, it's always great to send your speakers home with some swag as a token of your appreciation. A special sticker just for speakers at your meetup (for example: your group's logo in a different color scheme) can go a long way. 

## Planning a remote meetup 

### Getting started

- When you're ready to begin planning a remote meetup, please open an issue using our [meetups template](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=meetup-organizer). 
- If you would like to review this checklist and discuss your plans with a GitLab team member, please [schedule a Zoom call](https://calendly.com/jcoghlan) with GitLab's Evangelist Program Manager.

### What to do ASAP 

- Set the format. While in-person meetups tend to follow a familiar format, we encourage you to think differently when planning a remote event. Discussion groups, workshops, presentations, and more can all be conducted virtually. Think about the goal for the event and plan for that goal. 
- Pick your platform. At GitLab, we use Zoom for our remote meetings but we encourage you to find a tool that best fits your needs. Other platforms to consider are [Jitsi Meet](https://jitsi.org/jitsi-meet/) and [Google Meet](https://meet.google.com/?authuser=1) (requires a G-Suite account). For more options, Wired has compiled a [list of the best video conference apps](https://www.wired.co.uk/article/best-video-conference-apps).
- Set the date. 
- Set up an event page. If you intend to use Meetup.com, you can follow [these steps](https://www.meetup.com/blog/how-to-host-an-online-event-on-meetup/) to create an online event for your Meetup group. We are happy to connect your group with our [Meetup Pro network](https://www.meetup.com/pro/gitlab). Leave a comment on the event issue if you'd like to leverage our Meetup page to get your group started.  
- Apply a Code of Conduct to your group to ensure the safety and comfort of all participants. [GitLab's Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/) is a good option.
- Promote the event to your target audience. We recommend sharing on Reddit, Twitter, LinkedIn, Facebook, and other social channels to reach your audience. Use appropriate hashtags to reach people outside your network of followers. You may also want to promote the event on tech mailing lists or websites that focus on your area. 
- - For GitLab team members only: follow [the steps in our marketing handbook](/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents) to add your event to [about.gitlab.com/events](/events). You can assign the merge request to the Evangelist Program Manager (@johncoghlan) to merge. 

### What to do one week before your event

- Confirm plans for the event with any speaker(s). This is a good time to discuss how the speakers plan to present and confirm they have solid internet connection, have installed the meeting software and familiarized themselves with how to use it.
- Send a reminder to your guests about the event and encourage them to help you promote it. Include a simple ask of "share this with your colleagues, friends, and on social media".

### What to do the day of your event

- Send reminder to your guests before 1200 local time. Include an agenda and any other important information.
- Have any speakers join the call 30 min before the start time so you can brief them on their responsibilities, ensure all presenters' have their AV setups and internet connections working properly, and answer any questions. 
- At the start of the meetup, organizers should:
  - Introduce yourself
  - Welcome and thank the attendees
  - Ask people to turn on their cameras to create a better sense of community
  - Share important updates with the group
  - Review agenda
  - Introduce your speaker(s)
- For remote meetups, it is important for organizers to take additional steps to help familiarize your attendees with the video conference platform you choose. We recommend spending time during your introduction to provide an overview of the features you would like attendees to use. This may include:
  - **Breakout rooms**: The Zoom breakout room functionality is described in detail here: [Managing Video Breakout Rooms](https://support.zoom.us/hc/en-us/articles/206476313-Managing-Video-Breakout-Rooms). The host can assign attendees to specific rooms but there is no way to allow attendees to select a room. One workaround could be to ask folks to type their selection into the chat and then the host can assign them to their choice. For meetups in Zoom that use the breakout room feature, the host should explain the flow so folks that are new to this feature understand how to follow the prompts they will see on the screen. For example:
    - "Breakout rooms are small group conversations intended for casual conversation and building community. We hope everyone will participate. In order to get the conversation started, please start by sharing your name, location, what attracted you to the meetup, and your interests outside of GitLab." 
    - "When we do the breakout rooms, you will see a prompt asking you to join a room, click "Join breakout room" button to join your assigned room."
    - "After the allotted time, I will be close the rooms. You will see a 60-second countdown, that is the time to end conversations and return to the main room for closing remarks."
  - **Chat**: Encouraging folks to use the chat can help the attendees feel more engaged. It is helpful to newcomers if the host explains the chat functionality and provides guidance on how attendees should leverage this during the event (ex: ask folks to share their location, put  questions in the chat, etc.)
  - **Raise hand**: If you intend to use this feature, make sure you explain to attendees how to raise hand and ensure the presenter is prepared for questions during their talk. 
- To have some fun, we recommend the attendees unmute their microphones and give a round of applause to presenters upon completion of their talk.
- After the event, send a thank you to attendees and include a form for feedback and a reminder to RSVP for your next meetup (if one has been scheduled). 

### What to do the day after your event 

- Send thank you notes to your co-organizers, the speakers, and anyone else who helped you with the meetup. 

### Simple tips to take your remote meetup to the next level 

- Welcome early arrivals: Many folks will join your meetup early. It's a good idea to have a welcome slide on display or join early yourself and engage the early arrivals in conversation.
- Record your meetup: It can often be easier to record meetups when using video conferencing applications as many contain a feature that allows for recording meetings. We highly recommend recording your meetups and publishing the recordings to share with your community.  
- Keep it real: To have some fun, you can ask attendees unmute their microphones and give a round of applause to presenters upon completion of their talk.
- Speaker swag: when possible, it's always great to send your speakers home with some swag as a token of your appreciation. A special sticker just for speakers at your meetup (for example: your group's logo in a different color scheme) can go a long way. 

## Resources

### Logos 

GitLab logos can be found in our [press kit](https://about.gitlab.com/press/press-kit/#logos). 

### Templates 

These [communication templates](https://drive.google.com/drive/folders/1xglxuxFcxATpQ0ZajYvYu5aIyEK7TvTK?usp=sharing) can help you get a head start on your communications with your community. Templates include sample agendas, reminder and thank you emails, and boilerplate language about GitLab for your group or event descriptions. 
 
### Signs for your meetup 

Use [these signs](https://drive.google.com/open?id=1jO-MbZI21sXbzZYQ0u4ClghmNJFzsT7X) to help your members find what they need and free up your own time for deeper conversations then directing your guests to the bathroom or pizza. 

### Feedback form

Feedback is critical. A simple [three question survey](https://docs.google.com/forms/d/167QK2Oudqrdu_hpRCK45G7WDW65IDV63XWgjKa6Srrw/edit) can help you improve your group and gives your members a chance to offer feedback.
