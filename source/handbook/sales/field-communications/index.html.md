---
layout: handbook-page-toc
title: "Field Communications"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Field Communications Handbook

This team is responsible for creating effective, timely, and easily consumable communications with Sales and Customer Success audiences. Our goal is to help the field sell better, faster, and smarter with communication programs that keep them better informed of organizational/business updates that impact their roles, as well as useful resources that will enable day-to-day work. 

This page is a work in progress, (WIP) and we will update it as we add more Field Communications-related content to the Handbook. 

For more information about the Field Enablement team, visit the [team handbook page.](/handbook/sales/field-operations/field-enablement/)

### Sharing Feedback
Ongoing feedback and participation from the field is imperative to the success of the Field Communications team and its programs. If you have feedback on the current processes or programs, requests for a certain type of content, and/or ideas for ways we can further improve communication with the field, please follow this process: 

- To share feedback or ideas, submit an issue using [this issue template](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/boards/1640366?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=FC%20Feedback)
- Field Communications will triage feedback as soon as possible, given availability and bandwidth. Additional stakeholders and subject matter experts will be pulled in as appropriate.
- During this review, Field Communications will determine next-steps for each issue submitted, placing it into one of three categories: 
   1. Accepted (tag: `FC Feedback::Accepted`) - Field Communications feedback that will be actioned on
   1. Deferred (tag: `FC Feedback::Deferred`) - Field Communications feedback that will be deferred until more information is gathered or until more bandwidth becomes available
   1. Declined (tag: `FC Feedback::Declined`) - Field Communications feedback that is declined (no action will be taken)
- Field Communications will respond to each issue with rationale behind its disposition and will keep issue stakeholders updated on progress or completion if the issue is accepted or deferred.

### Key Field Communications Programs
- [Field Flash Newsletter](/handbook/sales/field-communications/field-flash-newsletter/)
